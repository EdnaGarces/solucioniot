﻿using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SolucionIoT.BIZ
{
    public abstract class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal IGenericRepository<T> Repository;
        public GenericManager(IGenericRepository<T> repository)
        {
            this.Repository = repository;
        }
        public IEnumerable<T> ObtenerTodos => Repository.Read;
        string error;

        public string Error => Repository.Error;
        
        public T Actualizar(T entidad)
        {
            return Repository.Update(entidad);
        }

        public T BuscarPOrId(string id)
        {
            return Repository.SearchById(id);
        }

        public bool Eliminar(string id)
        {
            return Repository.Delete(id);
        }

        public T Insertar(T entidad)
        {
            return Repository.Create(entidad);
        }
    }
}
