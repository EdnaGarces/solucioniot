﻿using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SolucionIoT.BIZ
{
    public class DispositivoManager : GenericManager<Dispositivo>, IDsipositivoManager
    {
        IGenericRepository<Usuario> usuarioRepository;
        public DispositivoManager(IGenericRepository<Dispositivo> repository, IGenericRepository<Usuario> usuarioRepository) : base(repository)
        {
            this.usuarioRepository = usuarioRepository;
        }

        public IEnumerable<Dispositivo> DispositivosDeUsuarioPorEmail(string email)
        {
            Usuario user = usuarioRepository.Query(u => u.Correo == email).SingleOrDefault();
            return DispositivosDeUsuarioPorId(user.Id);
        }

        public IEnumerable<Dispositivo> DispositivosDeUsuarioPorId(string id)
        {
            return Repository.Query(u => u.Id == id);
        }
    }
}
