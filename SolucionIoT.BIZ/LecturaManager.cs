﻿using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SolucionIoT.BIZ
{
    public class LecturaManager : GenericManager<Lectura>, ILecturaManager
    {
        public LecturaManager(IGenericRepository<Lectura> repository) : base(repository)
        {
        }

        public IEnumerable<Lectura> LecturasDelDispositivo(string id)
        {
            return Repository.Query(l => l.IdDispositivo == id);
        }

        public IEnumerable<Lectura> LecturasDelDispositivo(string id, DateTime inicio, DateTime fin)
        {
            return Repository.Query(l => l.IdDispositivo == id && l.FechaHora>=inicio && l.FechaHora<=fin);
        }
    }
}
