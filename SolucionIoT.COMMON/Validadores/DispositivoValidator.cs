﻿using FluentValidation;
using SolucionIoT.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace SolucionIoT.COMMON.Validadores
{
   public  class DispositivoValidator:GenericValidator<Dispositivo>
    {
        public DispositivoValidator() 
        {
            RuleFor(d=>d.IdUsuario).NotEmpty().NotNull();
            RuleFor(d=>d.Ubicacion).NotEmpty().NotNull().Length(3,50);
            RuleFor(d=>d.UsoRelevado1).NotEmpty().NotNull().Length(3,50);
            RuleFor(d=>d.UsoRelevado2).NotEmpty().NotNull().Length(3,50);
            RuleFor(d=>d.UsoRelevado3).NotEmpty().NotNull().Length(3,50);
            RuleFor(d=>d.UsoRelevado4).NotEmpty().NotNull().Length(3,50);
            RuleFor(d=>d.UsoBuzzer).NotEmpty().NotNull().Length(3,50);
            RuleFor(d=>d.UbucacionPIR).NotEmpty().NotNull().Length(3,50);

        }
    }
}
