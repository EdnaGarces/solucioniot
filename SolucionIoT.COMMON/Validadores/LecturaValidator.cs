﻿using FluentValidation;
using SolucionIoT.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace SolucionIoT.COMMON.Validadores
{
    public class LecturaValidator:GenericValidator<Lectura>
    {
        public LecturaValidator()
        {
            RuleFor(l=>l.Temperatura).NotEmpty().NotNull();
            RuleFor(l=>l.Humedad).NotEmpty().NotNull();
            RuleFor(l=>l.Luminosidad).NotEmpty().NotNull();
            RuleFor(l=>l.IdDispositivo).NotEmpty().NotNull();
        }
    }
}
