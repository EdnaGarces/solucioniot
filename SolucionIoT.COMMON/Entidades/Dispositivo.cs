﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolucionIoT.COMMON.Entidades
{
    public class Dispositivo : BaseDTO
    {
        public string IdUsuario { get; set; }
        public string Ubicacion { get; set; }
        public string Notas { get; set; }
        public string UsoRelevado1 { get; set; }
        public string UsoRelevado2 { get; set; }
        public string UsoRelevado3 { get; set; }
        public string UsoRelevado4 { get; set; }
        public string UsoBuzzer { get; set; }
        public string  UbucacionPIR { get; set; }


    }
}
